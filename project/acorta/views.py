from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from .models import Url


# Check which number are we, in order to assign the next one.
def assignNumber():
    i = 1
    for e in Url.objects.all():
        try:
            if Url.objects.get(shortUrl=i):
                i += 1
        except Url.DoesNotExist:
            return i


# Load the URL Shortener 'homepage' template in the shortener(request) function.
def homepage(request):
    # 1. Get the item list
    url_list = Url.objects.all()
    # 2. Load the template
    template = loader.get_template('acorta/url_index.html')
    # 3. Link the template variables to the Python ones.
    context = {
        'url_list': url_list,
    }
    # 4. Render --> execute 3.
    return HttpResponse(template.render(context, request))


# Reformat the URL's: adding 'https://' in case they don't have it.
def validUrls(url):
    complete_url = 'https://'
    if url.startswith('https://') or url.startswith('http://'):
        complete_url = url
    else:
        complete_url += url

    return complete_url


# Extract the user custom URL and the real one from the form.
# We return the Shortener URL 'homepage', letting the user make more entries or update existing ones.
@csrf_exempt
def shortener(request):
    if request.method == "POST":
        url = request.POST['url']
        url = validUrls(url)
        short = request.POST['short']
        try:
            # In case the URL is already in the database we update the short URL.
            urlContent = Url.objects.get(realUrl=url)
            urlContent.shortUrl = short
        except Url.DoesNotExist:
            # In case the URL is NOT in the database we create the new entry.
            urlContent = Url(realUrl=url, shortUrl=short)
            urlContent.realUrl = url
            urlContent.shortUrl = short
        # In both cases we save the object in the database.
        urlContent.save()
        # When there is NOT a custom URL (space blank).
        if urlContent.shortUrl == '':
            urlContent.shortUrl = assignNumber()
            urlContent.save()

    return homepage(request)


# Function for redirecting to the existing resource or the 'homepage'.
# We will use a template for this purpose, with the same steps as the previous one.
@csrf_exempt
def redirect(request, resource):
    try:
        # In case the resource exists.
        url_list = Url.objects.get(shortUrl=resource)
    except:
        # In case the resource does NOT exist.
        url_list = None
    # Load template.
    template = loader.get_template('acorta/redirection.html')
    # Link the variables.
    context = {
        'url_list': url_list,
        'resource': resource
    }
    # Send a different HTTP response depending on or the resource.
    if url_list is None:
        return HttpResponseNotFound(template.render(context, request))
    else:
        return HttpResponse(template.render(context, request))
