from django.urls import path
from . import views

urlpatterns = [
    path('', views.shortener),
    path('<str:resource>/', views.redirect),
    path('<str:resource>', views.redirect),
]
